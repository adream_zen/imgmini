'use strict'

import gulp			from 'gulp';
import config		from './tasks/config';
import runSequence 	from 'run-sequence';

// gulp tasks

import clean	from "./tasks/clean";
import images	from "./tasks/images";


gulp.task('images',		images);
gulp.task('clean',		clean);

gulp.task('default', ['clean','images']);

gulp.task('image',['clean','images']);