'use strict';

import gulp			from 'gulp';
import config		from './config';
import reportErr	from './notify';

import image 		from 'gulp-image';
import imageResize 	from 'gulp-image-resize';

export default () => {
	return gulp.src(config.images.source)
		.pipe(imageResize({
				width : 	config.images.width,
				heigh : 	config.images.height,
				crop : 		config.images.crop,
				upscale : 	false
			}).on('error', function(error) {
				reportErr(error, this);
			})
		)
		.pipe(
			image({
				pngquant: true,
				optipng: true,
				zopflipng: false,
				jpegRecompress: false,
				jpegoptim: true,
				mozjpeg: true,
				guetzli: false,
				gifsicle: true,
				svgo: true,
				concurrent: 10
			})
		)
		.pipe(gulp.dest(config.images.destination))
		.on('error', function(error) {
			reportErr(error, this);
		});
}